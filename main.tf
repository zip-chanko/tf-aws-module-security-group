resource "aws_security_group" "sg" {
  name        = var.name
  description = var.description != null ? var.description : var.name
  vpc_id      = var.vpc_id

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  to_port           = 0
  from_port         = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg.id
}

resource "aws_security_group_rule" "ingress" {
  for_each = var.allow_rules

  type                     = "ingress"
  from_port                = each.value.port_number
  to_port                  = each.value.port_number
  protocol                 = each.value.protocol
  cidr_blocks              = each.value.ip_range
  source_security_group_id = each.value.security_group
  security_group_id        = aws_security_group.sg.id
}
