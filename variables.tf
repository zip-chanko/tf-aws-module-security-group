variable "name" {
  description = "Name of the security group."
  type        = string
}

variable "description" {
  description = "Description of the security group."
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "VPC Id of the security group."
  type        = string
}

variable "allow_rules" {
  description = "List of objects for ingress rule."
  type = map(object({
    ip_range       = optional(list(string), null)
    port_number    = number
    protocol       = optional(string, "tcp")
    security_group = optional(string, null)
  }))
}
